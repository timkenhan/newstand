/*
 *To run this script, run with:
 *  $ node aggregate.js <url-list>.txt <output-path>.json
 * e.g.: $ node aggregate.js url-list.txt public/feed_data.json
 */

const MAX_DESCRIPTION_LEN = 200;

const INPUT_FILE = process.argv[2];
const OUTPUT_FILE = process.argv[3];
const MAX_ENTRIES = process.argv[4] || 20;

const fs = require('fs').promises;
const xmlParser = require('xml2js').parseStringPromise;
const sanitizeHtml = require('sanitize-html');

let result = {};

const processDescription = str => {
  const temp = sanitizeHtml(str, {allowedTags:[]});
  return (temp.length > MAX_DESCRIPTION_LEN ? temp.substring(0, MAX_DESCRIPTION_LEN) + '...' : temp );
}

(async () => {
  const url_list = (await fs.readFile(INPUT_FILE,{encoding:'utf-8'})).split('\n');
  for(const url of url_list) {
    if (!url) continue;
    const response = await fetch(url)
    const feed = await xmlParser(await response.text(), {explicitArray:false});
    const items = feed.rss.channel.item
    const title = feed.rss.channel.title;
    result[title] = items.slice(-MAX_ENTRIES).map(entry => ({
        title: entry.title, 
        description: processDescription(entry.description),
        pubDate: entry.pubDate,
        link: entry.link,
    }));
  }
  fs.writeFile(OUTPUT_FILE, JSON.stringify(result));
})();
